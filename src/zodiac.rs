use crate::{constants, LunarCalError, LunarCalResult};
use std::{convert::TryFrom, fmt};

/// Cycle of 60 terms derived from 12 Eathly Branches and 10 Heavenly Stems
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SexagenaryCycle {
    stem: HeavenlyStem,
    branch: EarthlyBranch,
}

/// Represents Earthly Branch, 'Địa Chi'. The twelve variants together form one half of 60-term
/// cycle.  Note that it differs from Chinese system upon which it is based.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum EarthlyBranch {
    Ty = 1,
    Suu,
    Dan,
    Mao,
    Thin,
    // Technically 'tỵ' spelling is preferred, but this would cause conflict
    // with 'tý' (rat, first Earthly Branch)
    Ti,
    Ngo,
    Mui,
    Than,
    Dau,
    Tuat,
    Hoi,
}

/// Represents Earthly Branch, 'Địa Chi'. The twelve variants together form one half of 60-term
/// cycle.  Note that it differs from Chinese system upon which it is based.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum HeavenlyStem {
    Giap = 1,
    At,
    Binh,
    Dinh,
    Mau,
    Ky,
    Canh,
    Tan,
    Nham,
    Quy,
}

impl SexagenaryCycle {
    pub fn new(stem: HeavenlyStem, branch: EarthlyBranch) -> Self {
        Self { stem, branch }
    }

    pub fn from_idx(stem_idx: u8, branch_idx: u8) -> LunarCalResult<Self> {
        let stem = HeavenlyStem::try_from(stem_idx)?;
        let branch = EarthlyBranch::try_from(branch_idx)?;
        Ok(Self::new(stem, branch))
    }
}

impl fmt::Display for SexagenaryCycle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.stem, self.branch)
    }
}

impl TryFrom<u8> for EarthlyBranch {
    type Error = LunarCalError;

    fn try_from(idx: u8) -> Result<Self, Self::Error> {
        match idx {
            x if x == Self::Ty as u8 => Ok(Self::Ty),
            x if x == Self::Suu as u8 => Ok(Self::Suu),
            x if x == Self::Dan as u8 => Ok(Self::Dan),
            x if x == Self::Mao as u8 => Ok(Self::Mao),
            x if x == Self::Thin as u8 => Ok(Self::Thin),
            x if x == Self::Ti as u8 => Ok(Self::Ti),
            x if x == Self::Ngo as u8 => Ok(Self::Ngo),
            x if x == Self::Mui as u8 => Ok(Self::Mui),
            x if x == Self::Than as u8 => Ok(Self::Than),
            x if x == Self::Dau as u8 => Ok(Self::Dau),
            x if x == Self::Tuat as u8 => Ok(Self::Tuat),
            x if x == Self::Hoi as u8 => Ok(Self::Hoi),
            _ => Err(LunarCalError::ZodiacOutOfBounds),
        }
    }
}

impl fmt::Display for EarthlyBranch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let name = constants::EARTHLY_BRANCHES[((*self as u8) - 1) as usize];
        write!(f, "{}", name)
    }
}

impl TryFrom<u8> for HeavenlyStem {
    type Error = LunarCalError;

    fn try_from(idx: u8) -> Result<Self, Self::Error> {
        match idx {
            x if x == Self::Giap as u8 => Ok(Self::Giap),
            x if x == Self::At as u8 => Ok(Self::At),
            x if x == Self::Binh as u8 => Ok(Self::Binh),
            x if x == Self::Dinh as u8 => Ok(Self::Dinh),
            x if x == Self::Mau as u8 => Ok(Self::Mau),
            x if x == Self::Ky as u8 => Ok(Self::Ky),
            x if x == Self::Canh as u8 => Ok(Self::Canh),
            x if x == Self::Tan as u8 => Ok(Self::Tan),
            x if x == Self::Nham as u8 => Ok(Self::Nham),
            x if x == Self::Quy as u8 => Ok(Self::Quy),
            _ => Err(LunarCalError::ZodiacOutOfBounds),
        }
    }
}

impl fmt::Display for HeavenlyStem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let name = constants::HEAVENLY_STEMS[((*self as u8) - 1) as usize];
        write!(f, "{}", name)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_usize() {
        let first = 1;
        let maybe_rat = EarthlyBranch::try_from(first);
        assert!(maybe_rat.is_ok());
        let rat = maybe_rat.unwrap();
        assert_eq!(first, rat as u8);
    }

    #[test]
    fn cycle_to_string() {
        let cyle = SexagenaryCycle::new(HeavenlyStem::Canh, EarthlyBranch::Ngo);
        assert_eq!(&cyle.to_string(), "Canh Ngọ")
    }
}
