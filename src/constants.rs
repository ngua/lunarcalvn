use crate::zodiac;
use lazy_static::lazy_static;
use std::convert::TryFrom;

lazy_static! {
    pub static ref SEXAGENARY_YEARS: Box<[Option<zodiac::SexagenaryCycle>]> = {
        let sexagenary_years = (0..60).fold(Vec::new(), |mut acc, curr| {
            {
                let stem = zodiac::HeavenlyStem::try_from(((curr % 10) + 1) as u8).ok();
                let branch = zodiac::EarthlyBranch::try_from(((curr % 12) + 1) as u8).ok();
                let result = if let (Some(stem), Some(branch)) = (stem, branch) {
                    Some(zodiac::SexagenaryCycle::new(stem, branch))
                } else {
                    None
                };
                acc.push(result);
            }
            acc
        });
        sexagenary_years.into_boxed_slice()
    };
}

pub const EARTHLY_BRANCHES: [&str; 12] = [
    "Tý", "Sửu", "Dần", "Mão", "Thìn", "Tỵ", "Ngọ", "Mùi", "Thân", "Dậu", "Tuất", "Hợi",
];

pub const HEAVENLY_STEMS: [&str; 10] = [
    "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ", "Canh", "Tân", "Nhâm", "Quý",
];

pub const VI_MONTHS: [&str; 12] = [
    "Một",
    "Hai",
    "Ba",
    "Tư",
    "Năm",
    "Sáu",
    "Bảy",
    "Tám",
    "Chín",
    "Mười",
    "Mười Một",
    "Mười Hai",
];

pub const SOLAR_TERMS: [&str; 25] = [
    "Xuân phân",
    "Thanh minh",
    "Cốc vũ",
    "Lập hạ",
    "Tiểu mãn",
    "Mang chủng",
    "Hạ chí",
    "Tiểu thử",
    "Đại thử",
    "Lập thu",
    "Xử thử",
    "Bạch lộ",
    "Thu phân",
    "Hàn lộ",
    "Sương giáng",
    "Lập đông",
    "Tiểu tuyết",
    "Đại tuyết",
    "Đông chí",
    "Đông chí",
    "Tiểu hàn",
    "Đại hàn",
    "Lập xuân",
    "Vũ thủy",
    "Kinh trập",
];

pub const SOLAR_TERM_RANGES: [[(u32, u32); 2]; 25] = [
    [(3, 21), (4, 5)],
    [(4, 5), (4, 20)],
    [(4, 20), (5, 6)],
    [(5, 6), (5, 21)],
    [(5, 21), (6, 6)],
    [(6, 6), (6, 21)],
    [(6, 21), (7, 7)],
    [(7, 7), (7, 23)],
    [(7, 23), (8, 7)],
    [(8, 7), (8, 23)],
    [(8, 23), (9, 8)],
    [(9, 8), (9, 23)],
    [(9, 23), (10, 8)],
    [(10, 8), (10, 23)],
    [(10, 23), (11, 7)],
    [(11, 7), (11, 22)],
    [(11, 22), (12, 7)],
    [(12, 7), (12, 22)],
    [(12, 22), (12, 31)],
    // Have to handle wrapping around to Jan 1 explicitly
    [(1, 1), (1, 6)],
    [(1, 6), (1, 21)],
    [(1, 21), (2, 4)],
    [(2, 4), (2, 19)],
    [(2, 19), (3, 5)],
    [(3, 5), (3, 21)],
];
