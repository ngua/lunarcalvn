/// Module for constructing and representing lunar dates.
/// 'Lunar' is a misnomer, as the Vietnamese lunar calendar (Âm Lịch)
/// is really a lunisolar calendar
use crate::{constants, convert, convert::JulianDayNumber, zodiac, LunarCalError, LunarCalResult};
use chrono::{prelude::*, NaiveDate, Weekday};
use derive_deref::Deref;
use std::{convert::TryFrom, fmt};

/// Represents single date in Lunar calendar
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct LunarDate {
    pub year: LunarYear,
    pub month: LunarMonth,
    pub day: LunarDay,
    pub season: LunarSeason,
    pub solar_term: SolarTerm,
}

/// Newtype wrapper around u32 to represent Lunar year.
#[derive(Debug, Copy, Clone, PartialEq, Deref)]
pub struct LunarYear(i32);

/// Represents single lunar month. A total of 14 lunar months
/// may appear in a given year; a leap month occurs every three
/// years in the lunar calendar, which takes the name of the
/// preceding non-leap month
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct LunarMonth {
    number: u32,
    is_leap: bool,
}

/// Represents single lunar day, including its Julian day number (jdn) and weekday.
/// The jdn is required to calculate the cycle for the day, so it makes sense to
/// keep it as a field rather than on the parent `LunarDate` struct
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct LunarDay {
    number: u32,
    weekday: LunarWeekday,
    jdn: JulianDayNumber,
}

/// Newtype wrapper to re-implement `fmt::Display` using Vietnamese
/// weekday names
#[derive(Debug, Copy, Clone, PartialEq, Deref)]
struct LunarWeekday(Weekday);

/// Season assigned to each Lunar date
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LunarSeason {
    Winter,
    Spring,
    Summer,
    Fall,
}

/// One of 24 solar terms ("tiết khí"), spaced evenly
/// every 15° along ecliptic. Based on solar date, not
/// lunar, used to synchronize lunisolar calendar with
/// seasons. The precise term only deviates one or two days
/// in each solar year.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SolarTerm {
    name: &'static str,
}

impl LunarDate {
    fn new(
        year: LunarYear,
        month: LunarMonth,
        day: LunarDay,
        season: LunarSeason,
        solar_term: SolarTerm,
    ) -> Self {
        Self {
            year,
            month,
            day,
            season,
            solar_term,
        }
    }
}

impl fmt::Display for LunarDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{:04}-{:02}-{:02}",
            *self.year, self.month.number, self.day.number
        )
    }
}

impl LunarYear {
    pub fn new(number: i32) -> Self {
        Self(number)
    }

    pub fn cycle(&self) -> LunarCalResult<zodiac::SexagenaryCycle> {
        let sub = **self - 3;
        let div = sub / 60;
        let rem = sub - (div * 60);
        Ok(constants::SEXAGENARY_YEARS[(rem as usize) - 1]
            .ok_or(LunarCalError::ZodiacOutOfBounds)?)
    }
}

impl LunarMonth {
    pub fn new(number: u32, is_leap: bool) -> Self {
        Self { number, is_leap }
    }

    pub fn cycle(&self, year: LunarYear) -> LunarCalResult<zodiac::SexagenaryCycle> {
        let stem_idx = (((*year * 12 + (self.number as i32) + 3) % 10) + 1) as u8;
        let branch_idx = (((self.number + 1) % 12) + 1) as u8;
        Ok(zodiac::SexagenaryCycle::from_idx(stem_idx, branch_idx)?)
    }
}

impl fmt::Display for LunarMonth {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let month_name = constants::VI_MONTHS[(self.number - 1) as usize];
        if self.is_leap {
            write!(f, "Tháng {} nhuận", month_name)
        } else {
            write!(f, "Tháng {}", month_name)
        }
    }
}

impl LunarDay {
    pub fn new(number: u32, weekday: Weekday, jdn: JulianDayNumber) -> Self {
        Self {
            number,
            weekday: LunarWeekday::new(weekday),
            jdn,
        }
    }

    pub fn cycle(&self) -> LunarCalResult<zodiac::SexagenaryCycle> {
        let stem_idx = (((*self.jdn + 9) % 10) + 1) as u8;
        let branch_idx = (((*self.jdn + 1) % 12) + 1) as u8;
        Ok(zodiac::SexagenaryCycle::from_idx(stem_idx, branch_idx)?)
    }
}

impl fmt::Display for LunarDay {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.weekday.to_string())
    }
}

impl TryFrom<&NaiveDate> for LunarDate {
    type Error = LunarCalError;

    /// Convert from Gregorian date (represented as `chrono::NaiveDate`)
    fn try_from(solar_date: &NaiveDate) -> Result<Self, Self::Error> {
        use std::cmp::Ordering;

        let solar_year = solar_date.year();

        let jdn = JulianDayNumber::try_from(solar_date)?;
        let k = (((jdn.as_f64() - 2_415_021.076_998_695) / 29.530_588_853).floor()) as i32;

        let month_start = {
            let nm = convert::new_moon_day(k + 1);
            if nm > jdn {
                convert::new_moon_day(k)
            } else {
                nm
            }
        };

        let eleventh = convert::eleventh_lunar_month(solar_year)?;

        let (first_11th, next_11th, lunar_year) = match eleventh.cmp(&*month_start) {
            Ordering::Greater | Ordering::Equal => (
                convert::eleventh_lunar_month(solar_year - 1)?,
                eleventh,
                solar_year,
            ),
            Ordering::Less => (
                eleventh,
                convert::eleventh_lunar_month(solar_year + 1)?,
                solar_year + 1,
            ),
        };

        let lunar_day = (*jdn - *month_start) as u32 + 1;

        let diff = ((month_start.as_f64() - first_11th.as_f64()) / 29_f64).floor();
        let diff = diff as i32;

        let mut lunar_leap = false;
        let mut lunar_month = (diff + 11) as u32;

        if *next_11th - *first_11th > 365 {
            let leap_month_diff = convert::leap_month_offset(first_11th)?;

            if diff >= leap_month_diff {
                lunar_month = (diff as u32) + 10;
                if diff == leap_month_diff {
                    lunar_leap = true;
                }
            }
        }

        if lunar_month > 12 {
            lunar_month -= 12;
        }

        let lunar_year = if lunar_month >= 11 && diff < 4 {
            lunar_year - 1
        } else {
            lunar_year
        };

        let weekday = solar_date.weekday();
        let lunar_month = LunarMonth::new(lunar_month, lunar_leap);
        let lunar_year = LunarYear::new(lunar_year);
        let lunar_day = LunarDay::new(lunar_day, weekday, jdn);
        let lunar_season = LunarSeason::new(lunar_month);
        let solar_term = SolarTerm::from(solar_date);

        Ok(LunarDate::new(
            lunar_year,
            lunar_month,
            lunar_day,
            lunar_season,
            solar_term,
        ))
    }
}

impl LunarWeekday {
    fn new(weekday: Weekday) -> Self {
        Self(weekday)
    }
}

impl fmt::Display for LunarWeekday {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let day_name = match **self {
            Weekday::Mon => "Hai",
            Weekday::Tue => "Ba",
            Weekday::Wed => "Tư",
            Weekday::Thu => "Năm",
            Weekday::Fri => "Sáu",
            Weekday::Sat => "Bảy",
            Weekday::Sun => "Chủ Nhật",
        };
        // Sunday is a special case that does not follow the naming pattern
        // of other weekdays in Vietnamese
        if let Weekday::Sun = **self {
            write!(f, "{}", day_name)
        } else {
            write!(f, "Thứ {}", day_name)
        }
    }
}

impl LunarSeason {
    fn new(month: LunarMonth) -> Self {
        match month.number % 12 {
            0..=2 => Self::Spring,
            3..=5 => Self::Summer,
            6..=8 => Self::Fall,
            9..=11 => Self::Winter,
            _ => unreachable!(),
        }
    }
}

impl fmt::Display for LunarSeason {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let name = match self {
            Self::Spring => "Xuân",
            Self::Summer => "Hạ",
            Self::Fall => "Thu",
            Self::Winter => "Đông",
        };
        write!(f, "Mùa {}", name)
    }
}

impl fmt::Display for SolarTerm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Tiết {}", self.name)
    }
}

impl From<&NaiveDate> for SolarTerm {
    fn from(date: &NaiveDate) -> Self {
        let date = *date;

        let cmp_date = |m, d| NaiveDate::from_ymd(date.year(), m, d);
        let in_date_range = |cmp_begin: (u32, u32), cmp_end: (u32, u32)| {
            let begin = cmp_date(cmp_begin.0, cmp_begin.1);
            let end = cmp_date(cmp_end.0, cmp_end.1);
            date >= begin && date < end
        };

        let range = &mut constants::SOLAR_TERM_RANGES.iter().enumerate();

        let _ = range.try_fold(0, |_, (i, curr)| {
            let is_in_range = in_date_range(curr[0], curr[1]);

            if is_in_range {
                None
            } else {
                Some(i)
            }
        });

        let idx = range.next().map(|r| r.0 - 1).unwrap_or(0);

        SolarTerm {
            name: constants::SOLAR_TERMS[idx],
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::zodiac;
    use chrono::{NaiveDate, Weekday};
    use std::convert::{From, TryFrom};

    #[test]
    fn sol_to_lun() {
        let date = NaiveDate::from_ymd(1970, 1, 1);
        let jdn = JulianDayNumber::try_from(&date).unwrap();
        let lunar_date = LunarDate::try_from(&date).unwrap();
        assert_eq!(
            lunar_date,
            LunarDate::new(
                LunarYear::new(1969),
                LunarMonth::new(11, false),
                LunarDay::new(24, Weekday::Thu, jdn),
                LunarSeason::new(LunarMonth::new(11, false)),
                SolarTerm::from(&date)
            )
        );
    }

    #[test]
    fn leap_month() {
        // This date should be a leap month in Lunar calendar
        let date = NaiveDate::from_ymd(1933, 6, 23);
        let lunar_date = LunarDate::try_from(&date).unwrap();
        assert!(lunar_date.month.is_leap);
    }

    #[test]
    fn leap_month_to_string() {
        let date = NaiveDate::from_ymd(1933, 6, 23);
        let lunar_date = LunarDate::try_from(&date).unwrap();
        assert_eq!("Tháng Năm nhuận", &lunar_date.month.to_string())
    }

    #[test]
    fn month_cycle() {
        let month = LunarMonth::new(3, false);
        let year = LunarYear::new(2004);
        assert_eq!(
            zodiac::SexagenaryCycle::new(zodiac::HeavenlyStem::Mau, zodiac::EarthlyBranch::Thin),
            month.cycle(year).unwrap()
        );
    }

    #[test]
    fn year_cycle() {
        let year = LunarYear(2020);
        assert_eq!(
            zodiac::SexagenaryCycle::new(zodiac::HeavenlyStem::Canh, zodiac::EarthlyBranch::Ty),
            year.cycle().unwrap()
        );
    }

    #[test]
    fn jdn_cycle() {
        let date = NaiveDate::from_ymd(1970, 1, 1);
        let jdn = JulianDayNumber::try_from(&date).unwrap();
        let day = LunarDay::new(24, Weekday::Thu, jdn);
        assert_eq!(
            zodiac::SexagenaryCycle::new(zodiac::HeavenlyStem::Tan, zodiac::EarthlyBranch::Ti),
            day.cycle().unwrap()
        );
    }

    #[test]
    fn solar_term() {
        let date = NaiveDate::from_ymd(2020, 1, 1);
        let solar_term = SolarTerm::from(&date);
        assert_eq!("Tiết Đông chí", &solar_term.to_string())
    }
}
