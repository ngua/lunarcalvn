use chrono::{NaiveDate, Utc};
use std::{convert::TryFrom, error::Error, io::Write};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
use thiserror::Error;

mod constants;
mod convert;
pub mod lunar;
pub mod zodiac;

pub type LunarCalResult<T> = Result<T, LunarCalError>;

pub fn convert(date: Option<NaiveDate>) -> LunarCalResult<(NaiveDate, lunar::LunarDate)> {
    // Get current utc date if no date argument provided
    let solar_date = if let Some(date) = date {
        date
    } else {
        Utc::today().naive_utc()
    };

    let lunar_date = lunar::LunarDate::try_from(&solar_date)?;

    Ok((solar_date, lunar_date))
}

pub fn display(solar_date: NaiveDate, lunar_date: lunar::LunarDate) -> Result<(), Box<dyn Error>> {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);

    stdout.set_color(ColorSpec::new().set_fg(Some(Color::Yellow)).set_bold(true))?;
    write!(&mut stdout, "\n{} Âm Dương", solar_date.to_string())?;

    stdout.set_color(ColorSpec::new().set_fg(Some(Color::Blue)).set_bold(true))?;
    writeln!(&mut stdout, "{:>20} Âm Lịch", lunar_date.to_string())?;

    stdout.set_color(ColorSpec::new().set_fg(Some(Color::Blue)).set_bold(false))?;
    writeln!(&mut stdout, "{:>47}", lunar_date.month.to_string())?;
    writeln!(&mut stdout, "{:>47}", lunar_date.day.to_string())?;
    writeln!(&mut stdout, "{:-^55}", "")?;
    writeln!(
        &mut stdout,
        "{:>47}",
        format!("Tháng {}", lunar_date.month.cycle(lunar_date.year)?)
    )?;
    writeln!(
        &mut stdout,
        "{:>47}",
        format!("Năm {}", lunar_date.year.cycle()?)
    )?;
    writeln!(
        &mut stdout,
        "{:>47}",
        format!("Ngày {}", lunar_date.day.cycle()?)
    )?;
    writeln!(&mut stdout, "{:>47}", lunar_date.solar_term.to_string())?;

    Ok(())
}

#[derive(Debug, Error)]
pub enum LunarCalError {
    #[error("Gregorian date cannot precede BC Nov. 23, 4713")]
    MininumSolarDate,
    #[error("Zodiac index out of bounds")]
    ZodiacOutOfBounds,
    #[error("Unknown error converting date")]
    Unknown,
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{NaiveDate, Weekday};
    use std::convert::TryFrom;

    #[test]
    fn correct_lunar_date() {
        let date = NaiveDate::from_ymd(1970, 1, 1);
        let lunar_date = lunar::LunarDate::try_from(&date).unwrap();
        let jdn = convert::JulianDayNumber::try_from(&date).unwrap();

        assert_eq!(lunar_date.year, lunar::LunarYear::new(1969));
        assert_eq!(&lunar_date.year.cycle().unwrap().to_string(), "Kỷ Dậu");

        assert_eq!(lunar_date.month, lunar::LunarMonth::new(11, false));
        assert_eq!(
            &lunar_date.month.cycle(lunar_date.year).unwrap().to_string(),
            "Bính Tý"
        );

        assert_eq!(lunar_date.day, lunar::LunarDay::new(24, Weekday::Thu, jdn));
        assert_eq!(&lunar_date.day.to_string(), "Thứ Năm");
        assert_eq!(&lunar_date.day.cycle().unwrap().to_string(), "Tân Tỵ");
        assert_eq!(&lunar_date.solar_term.to_string(), "Tiết Đông chí");
    }
}
