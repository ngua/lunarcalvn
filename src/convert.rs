/// Module to convert between naive dates and Julian day numbers (jdn).
/// The jdn is an integer value representing the elapsed days since the
/// beginning of the Julian period. It is required to calculate dates in
/// the Vietnamese lunar calendar (actually lunisolar).
/// Massive gratitude owed to Hồ Ngọc Đức, whose algorithms are used here,
/// and without which conversion between Gregorian dates and lunisolar dates
/// would not be possible. See
/// https://www.informatik.uni-leipzig.de/~duc/amlich/calrules_v02.html for more.
use crate::{LunarCalError, LunarCalResult};
use chrono::{prelude::*, NaiveDate};
use derive_deref::Deref;
use lazy_static::lazy_static;
use std::{
    collections::HashMap,
    convert::TryFrom,
    f64::consts::PI,
    i32,
    ops::{Add, Sub},
};

/// Jan 1 1900 as Julian day
#[cfg(test)]
static MIN_SOLAR_JULIAN_DAY: i32 = 2_415_021;
/// As float for greater precision when calculating new moon
static MIN_SOLAR_JULIAN_DAY_FLOAT: f64 = 2_415_020.759_33;
lazy_static! {
    /// Earliest Gregorian date for deriving JDN
    static ref MINIMUM_VALID_GREGORIAN_DATE: NaiveDate = NaiveDate::from_ymd(-4713, 11, 23);
}

/// Newtype wrapper around i32 value
#[derive(Debug, PartialEq, Copy, Clone, PartialOrd, Deref)]
pub struct JulianDayNumber(i32);

impl TryFrom<&NaiveDate> for JulianDayNumber {
    type Error = LunarCalError;

    /// Attempt to construct `JulianDayNumber` from `NaiveDate`,
    /// returning error if date is earlier than the minimum
    /// date for calculating a JDN (BC Nov. 23, 4713)
    fn try_from(date: &NaiveDate) -> Result<Self, Self::Error> {
        if date < &*MINIMUM_VALID_GREGORIAN_DATE {
            Err(Self::Error::MininumSolarDate)
        } else {
            let y = date.year();
            let m = date.month() as i32;
            let d = date.day() as i32;

            let a = (14 - m) / 12;
            let y = y + 4800 - a;
            let m = m + 12 * a - 3;

            let jdn = d + (153 * m + 2) / 5 + y * 365 + y / 4 - y / 100 + y / 400 - 32045;
            Ok(Self(jdn))
        }
    }
}

impl From<JulianDayNumber> for NaiveDate {
    /// Derive the Gregorian date from JDN
    #[allow(clippy::many_single_char_names)]
    fn from(jdn: JulianDayNumber) -> NaiveDate {
        let a = *jdn + 32044;
        let b = (4 * a + 3) / 146_097;
        let c = a - (b * 146_097) / 4;

        let d = (4 * c + 3) / 1461;
        let e = c - (1461 * d) / 4;
        let m = (5 * e + 2) / 153;

        let day = e - (153 * m + 2) / 5 + 1;
        let month = m + 3 - 12 * (m / 10);
        let year = b * 100 + d - 4800 + m / 10;

        NaiveDate::from_ymd(year as i32, month as u32, day as u32)
    }
}

impl From<i32> for JulianDayNumber {
    fn from(day: i32) -> Self {
        Self(day)
    }
}

impl Add for JulianDayNumber {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        JulianDayNumber::from(*self + *other)
    }
}

impl Sub for JulianDayNumber {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        JulianDayNumber::from(*self - *other)
    }
}

impl From<JulianDayNumber> for f64 {
    fn from(jdn: JulianDayNumber) -> Self {
        Self::from(*jdn)
    }
}

impl JulianDayNumber {
    pub fn as_f64(self) -> f64 {
        // Pass self by value - thanks clippy!
        *self as f64
    }
}

/// Calculates kth day of new moon as Julian day since 1900/1/1
pub fn new_moon_day(k: i32) -> JulianDayNumber {
    let k = f64::from(k);
    let t = k / 1236.85;
    let t2 = t * t;
    let t3 = t2 * t;
    let dr = PI / 180_f64;
    let jd1 =
        MIN_SOLAR_JULIAN_DAY_FLOAT + 29.530_588_68 * k + 0.000_117_8 * t2 - 0.000_000_155 * t3;
    // mean new moon
    let jd1 = jd1 + 0.00033 * ((166.56 + 132.87 * t - 0.009_173 * t2) * dr).sin();
    // sun's mean anomaly
    let m = 359.224_2 + 29.105_356_08 * k - 0.000_033_3 * t2 - 0.000_003_47 * t3;
    // moon's mean anomaly
    let mpr = 306.025_3 + 385.816_918_06 * k + 0.010_730_6 * t2 + 0.000_012_36 * t3;
    // moon's argument of latitude
    let f = 21.2964 + 390.670_506_46 * k - 0.001_652_8 * t2 - 0.000_002_39 * t3;

    let c1 = (0.173_4 - 0.000_393 * t) * (m * dr).sin() + 0.002_1 * (2_f64 * dr * m).sin()
        - (0.406_8 * (mpr * dr).sin() + 0.016_1 * (dr * 2_f64 * mpr).sin())
        - (0.000_4 * (dr * 3_f64 * mpr).sin())
        + (0.010_4 * (dr * 2_f64 * f).sin() - 0.005_1 * (dr * (m + mpr)).sin())
        - (0.007_4 * (dr * (m - mpr)).sin() + 0.000_4 * (dr * (2_f64 * f + m)).sin())
        - (0.000_4 * (dr * (2_f64 * f - m)).sin() - 0.000_6 * (dr * (2_f64 * f + mpr)).sin())
        + (0.001 * (dr * (2_f64 * f - mpr)).sin() + 0.000_5 * (dr * (2_f64 * mpr + m)).sin());

    let deltat = if t < -11_f64 {
        0.001 + 0.000_839 * t + 0.000_226_1 * t2 - 0.000_008_45 * t3 - 0.000_000_081 * t * t3
    } else {
        -0.000_278 + 0.000_265 * t + 0.000_262 * t2
    };

    let jd_new = (jd1 + c1 - deltat + 0.5).floor();

    JulianDayNumber::from(jd_new as i32)
}

pub fn eleventh_lunar_month(solar_year: i32) -> LunarCalResult<JulianDayNumber> {
    let solar_date = NaiveDate::from_ymd(solar_year, 12, 31);
    let jdn = JulianDayNumber::try_from(&solar_date)?;
    let offset = *jdn - 2_415_021;
    let k = (f64::from(offset) / 29.530_588_853).floor();
    let new_moon = new_moon_day(k as i32);
    let longitude = solar_longitude(new_moon);
    let new_moon = if longitude >= 9 {
        new_moon_day((k as i32) - 1)
    } else {
        new_moon
    };
    Ok(new_moon)
}

pub fn leap_month_offset(first_11th: JulianDayNumber) -> LunarCalResult<i32> {
    let k = ((first_11th.as_f64() - 2_415_021.076_998_695) / 29.530_588_853 + 0.5).floor();
    let k = k as i32;

    // Memoize calculation during iteration below
    let mut arcs = HashMap::new();
    let mut calculate_arc = |n| {
        if let Some(arc) = arcs.get(&n) {
            *arc
        } else {
            let arc = solar_longitude(new_moon_day(k + n));
            arcs.insert(n, arc);
            arc
        }
    };

    // Total number of possible iterations to
    // find offset
    let rounds = &mut (1..14);

    // Abuse `try_fold` slightly to increment count until
    // `arc` and `last` return the same
    let off = rounds.try_fold(1, |_, i| {
        let last = calculate_arc(i as i32);
        let arc = calculate_arc((i + 1) as i32);

        if arc == last {
            None
        } else {
            Some(i)
        }
    });

    let f = if let Some(off) = off {
        off as i32
    } else {
        // If not found, return next from iterator
        rounds.next().ok_or(LunarCalError::Unknown)?
    };

    Ok(f - 1)
}

/// Calculates the Sun's longitude to determine the 'Trung khí' of a given lunar month
fn solar_longitude(jdn: JulianDayNumber) -> i32 {
    let jdn = jdn.as_f64() - 0.5;
    let t = (jdn - 2_451_545_f64) / 36_525_f64;
    let t2 = t * t;
    // degree to radian
    let dr = PI / 180_f64;
    // mean anomaly in degrees
    let m = 357.529_10 + 35_999.050_30 * t - 0.000_155_9 * t2 - 0.000_000_48 * t * t2;
    // mean longitude in degrees
    let ml = 280.466_45 + 36_000.769_83 * t + 0.000_303_2 * t2;
    let dl = (1.914_600 - 0.004_817 * t - 0.000_014 * t2) * (dr * m).sin()
        + (0.019_993 - 0.000_101 * t) * (dr * 2_f64 * m).sin()
        + 0.000_290 * (dr * 3_f64 * m).sin();
    // true longitude, degrees
    let tl = (ml + dl) * dr;
    // normalized
    let norm = tl - PI * 2_f64 * (tl / (PI * 2_f64)).floor();
    let long = (norm / PI * 6_f64).floor();
    long as i32
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::NaiveDate;

    #[test]
    fn nd_to_jdn() {
        let date = NaiveDate::from_ymd(1900, 1, 1);
        let jdn = JulianDayNumber::try_from(&date).unwrap();
        assert_eq!(*jdn, MIN_SOLAR_JULIAN_DAY);

        let date = NaiveDate::from_ymd(1970, 1, 1);
        let jdn = JulianDayNumber::try_from(&date).unwrap();
        assert_eq!(*jdn, 2440588);
    }

    #[test]
    fn jdn_to_nd() {
        let jdn = JulianDayNumber::from(2440588);
        let date = NaiveDate::from(jdn);
        assert_eq!(date, NaiveDate::from_ymd(1970, 1, 1));
    }

    #[test]
    fn reject_date_too_early() {
        let date = NaiveDate::from_ymd(-4713, 11, 22);
        let jdn = JulianDayNumber::try_from(&date);
        assert!(jdn.is_err());
    }

    #[test]
    fn find_min_new_moon_as_jd() {
        // 0th new moon from minimum solar date
        let earliest_new_moon = new_moon_day(0);
        // The returned `JulianDayNumber` should be the same as the one
        // computed from Gregorian date
        let date = NaiveDate::from_ymd(1900, 1, 1);
        let jdn = JulianDayNumber::try_from(&date).unwrap();
        assert_eq!(*earliest_new_moon, *jdn);
    }

    #[test]
    fn solar_term_min_solar_date() {
        let jdn = JulianDayNumber::from(MIN_SOLAR_JULIAN_DAY);
        let longitude = solar_longitude(jdn);
        assert_eq!(longitude, 9);
    }

    #[test]
    fn new_moon() {
        let nmd = new_moon_day(1236);
        assert_eq!(nmd, JulianDayNumber(2451520));
    }
}
