use chrono::NaiveDate;
use clap::{App, Arg};
use lunarcalvn::{convert, display};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("lunarcalvn")
        .version("0.1")
        .author("Rory Tyler Hayford")
        .about("Convert from Gregorian date to Vietnamese Lunar Calendar")
        .arg(
            Arg::with_name("date")
                .short("d")
                .long("date")
                .help(
                    "Optional date to convert, in %Y-%m-%d format, e.g. 1970-1-1. \
                    If invalid or not supplied, today's date is calculated",
                )
                .takes_value(true),
        )
        .get_matches();

    let date = matches
        .value_of("date")
        .and_then(|date| NaiveDate::parse_from_str(date, "%Y-%m-%d").ok());

    let (today, lunar_today) = convert(date)?;
    display(today, lunar_today)?;
    Ok(())
}
